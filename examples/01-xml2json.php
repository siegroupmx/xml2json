#!/usr/bin/php
<?php
function proteger_cadena($a=NULL) {
	return htmlentities($a, ENT_QUOTES);
}

function desproteger_cadena_src( $cadena ) {
	$a= html_entity_decode($cadena, ENT_QUOTES);
	$a= str_replace("&", "&amp;", $a);
	return $a;
}
	
# desprotege omitiendo ciertas variables
function desproteger_cadena( $cadena ) {
	$out=$cadena;
	$out= html_entity_decode( $out, ENT_QUOTES );

	if( strchr( $out, "<" ) )
		$out= str_replace( "<", htmlentities("<", ENT_QUOTES), $out );
	if( strchr( $out, ">" ) )
		$out= str_replace( ">", htmlentities(">", ENT_QUOTES), $out );

	if( strchr( $out, "\n" ) )
		$out= str_replace( "\n", "<br>", $out );
	if( strchr( $out, "\t" ) )
		$out= str_replace( "\t", "&nbsp;&nbsp;&nbsp;", $out );

	return $out;
}

function desproteger_cadena_xml( $cadena ) {
	$out= utf8_decode(strip_tags(desproteger_cadena($cadena)));
	return $out;
}

function acento( $vocal ) {
	return "&". $vocal. "acute;";
}

function mbnumber_format( $n, $c, $del='.', $miles=',', $admin=false) {
	$d= explode(".", $n);
	$c= ($admin ? $c:(isset($_SESSION["round_decimals"]) ? $_SESSION["round_decimals"]:$c)); # numero de decimales, default: 2

	$centavos='';
	for( $i=0; $i<$c; $i++ )
		{
		if( $i<$c )
			$centavos .= (isset($d[1][$i]) ? $d[1][$i]:0);
		}

	$num= $d[0].$del.($admin ? $centavos:(isset($_SESSION["round_cero"]) ? '':$centavos));
	return number_format($num, $c, $del, $miles);
}

include( '../autoload.php' );

// $file= 'test.xml';
$file= file_get_contents('test.xml');
$pais= 174; // 174=panama

$json= new XML2JSON();
$json->convert($file, true, false, $pais);
// $json->convert($file, false, false, $pais);

if( $json->getError() )
	echo "\nError: ". $json->getError();
else {
	echo "\n\nExito...\n\n";
	$r= $json->getRespuesta();
	print_r($r);
}

echo "\n\nFin del programa...\n\n";
exit(0);
?>