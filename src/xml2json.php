<?php
class XML2JSON{
	private $sucess=NULL;
	private $error=NULL;
	private $error_code=NULL;
	private $paisSupport= array(
		"174"=>array( // panama
			"namespaces"=>NULL
		), 
		"188"=>array( // repdom
			"namespaces"=>NULL
		), 
		"151"=>array( // mexico
			"namespaces"=>NULL
		), 
		"47"=>array( // colombia
			"namespaces"=>NULL
		), 
		"177"=>array( // peru
			"namespaces"=>NULL
		), 
		"58"=>array( // ecuador
			"namespaces"=>NULL
		), 
		"60"=>array( // el salvador
			"namespaces"=>NULL
		)
	);

	/**
	* establece el codigo de exito
	*/
	public function setSucess($a=NULL) {
		$this->sucess= ($a ? $a:NULL);
	}

	/**
	* retorna el mensaje de exito
	*/
	public function getSucess() {
		return $this->sucess;
	}

	/**
	* establece el mensaje de error
	*
	* @param string el mensaje de error
	* @param string el codigo de error
	*/
	public function setError($a=NULL, $b=NULL) {
		$this->error= ($a ? $a:NULL); # mensaje de error
		$this->setErrorCode($b ? $b:NULL); # codigo de error
	}

	/**
	* establece el codigo de error
	*
	* @param string el codigo del error
	*/
	public function setErrorCode($a=NULL) {
		$this->error_code= ($a ? $a:NULL);
	}

	/**
	* retorna el error con codigo
	*
	* @return string error con codigo
	*/
	public function getError() {
		return ($this->error_code ? $this->error_code. ' - ':'').($this->error ? $this->error:'');
	}

	/**
	* retorna la respuesta
	*
	* @return string el mensaje de exito
	*/
	public function getRespuesta() {
		return $this->getSucess();
	}

	/**
	* lee una ruta del XML
	*
	* @param string ruta al archivo xml
	* @param boolean indica si el xml viene en raw
	* @param bollean indica si los datos vienen en base64 o no
	*/
	public function readPathfromXml($xmlInput=NULL, $rawData=false, $isEncode=false, $xPath=NULL) {
		if( !$xmlInput )	$this->setError("No indico el XML a leer", "V002");
		else if( !$rawData && !file_exists($xmlInput) )	$this->setError("El XML a validar no existe", "V003");
		else {
			$xmlSrc= ($rawData ? ($isEncode ? base64_decode($xmlInput):$xmlInput):$xmlInput); # si es raw=true viene el string, sino viene una RUTA
			$xml= new DOMDocument( "1.0", "UTF-8" );
			#$xml->preserveWhiteSpace= FALSE;
			$xml->formatOutput= true;
			#if( !$rawData ) 	$xml->load($xmlSrc); /*de la ruta URL*/
			#else 				$xml->loadXML($xmlSrc); /*de los datos del contenido*/
			$xml->loadXML($rawData ? $xmlSrc:file_get_contents($xmlSrc)); # se cambia a carga por string por seguridad
			$xp= new DOMXPath($xml);
			$xp->registerNamespace('biz', 'urn:bizlinks:names:specification:ubl:peru:schema:xsd:BizlinksAggregateComponents-1');
			$xp->registerNamespace('cac', 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2');
			$xp->registerNamespace('cbc', 'urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2');
			$xp->registerNamespace('ds', 'http://www.w3.org/2000/09/xmldsig#');
			$xp->registerNamespace('ext', 'urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2');
			$xp->registerNamespace('qdt', 'urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2');
			$xp->registerNamespace('sac', 'urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1');
			$xp->registerNamespace('ccts', 'urn:un:unece:uncefact:documentation:2');
			$xp->registerNamespace('udt', 'urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2');
			$xp->registerNamespace('xades', 'http://uri.etsi.org/01903/v1.3.2#');
			$xp->registerNamespace('xades141', 'http://uri.etsi.org/01903/v1.4.1#');

			$this->setSucess(isset($xp->query($xPath)->item(0)->nodeValue) ? $xp->query($xPath)->item(0)->nodeValue:0);
		}
	}

	public function isPaisSupported($a=NULL) {
		return (count($this->paisSupport[$a]) ? true:false);
	}

	public function getNameSpaces($a=NULL) {
		return (count($this->paisSupport[$a]["namespaces"]) ? $this->paisSupport[$a]["namespaces"]:0);
	}

	/**
	* convierte XML a JSON
	*
	* @param string ruta al archivo xml o string
	* @param boolean indica si el xml viene en raw
	* @param bollean indica si los datos vienen en base64 o no
	* @param string codigo de pais
	*/
	public function convert($a=NULL, $rawData=false, $isEncode=false, $p=NULL) {
		if( !$a )
			$this->setError("no indico archivo XML");
		else if( !$p )
			$this->setError("el pais de origen del XML no fue indicado");
		else if( !$this->isPaisSupported($p) )
			$this->setError("el pais indicado no esta soportado aun");
		else {
			$xmlSrc= ($rawData ? ($isEncode ? base64_decode($a):$a):$a); # si es raw=true viene el string, sino viene una RUTA
			$fileXml= ($rawData ? $a:file_get_contents($a));

			if( $this->getNameSpaces($p) ) { // con namespaces
				$xml= new DOMDocument( "1.0", "UTF-8" );
				#$xml->preserveWhiteSpace= FALSE;
				$xml->formatOutput= true;
				if( $rawData )
					$xml->load($fileXml);
				else
					$xml->loadXML($fileXml);
				$xp= new DOMXPath($xml);
				$ns= $this->getNameSpaces($p);

				foreach( $ns as $k=>$v ) {
					$xp->registerNamespace($k, $v);
				}

				// mas cosas por hacer ejm: print_r($xp->query($xPath)->item(0)->nodeValue);
			}
			else {
				$xml= simplexml_load_string($fileXml);	

				$this->setSucess($xml);
				$this->setError(NULL);
			}
		}
	}

	public function __construct() {
	}
}
?>